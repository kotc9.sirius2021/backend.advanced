﻿using System.Linq;
using Backend.Advanced.data;
using Backend.Advanced.Database;

namespace GraphqlTests
{
    public class TestHelper
    {
        public static void CreateData(BlogContext db)
        {
            db.Database.EnsureDeleted();
            db.Database.EnsureCreated();

            db.Users.AddRange(
                DatabaseData.Users.Select(c =>
                {
                    c.Avatar = $"https://localhost:5001{c.Avatar}";
                    return c;
                }));

            db.Posts.AddRange(DatabaseData.Posts);
            db.Comments.AddRange(DatabaseData.Comments);
            db.SaveChanges();
        }
    }
}