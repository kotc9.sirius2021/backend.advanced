﻿using System;
using System.Collections.Generic;
using System.Linq;
using Backend.Advanced.Database;
using Backend.Advanced.Models;
using Backend.Advanced.schemes;
using EntityGraphQL;
using GraphqlTests.Data;
using Xunit;
using Xunit.Abstractions;

namespace GraphqlTests
{
    public class UserTests
    {
        private readonly ITestOutputHelper _testOutputHelper;

        public UserTests(ITestOutputHelper testOutputHelper)
        {
            _testOutputHelper = testOutputHelper;
        }

        #region READ

        [Fact]
        public void User_GetData_ReturnJson()
        {
            var schema = BlogSchema.MakeSchema();
            var request = new QueryRequest
            {
                Query =
                    @"{
                        users {
                            id
                            name
                        }
                    }"
            };
            var db = new BlogContext();
            TestHelper.CreateData(db);
            var result = schema.ExecuteQuery(request, db, null, null);

            Assert.Empty(result.Errors);
        }

        #endregion

        #region CREATE

        [Theory]
        [InlineData("Yahiko", "Uzumaki", "yahiko@akatsuki.jp", "https://website/to/image/yahiko")]
        [InlineData("", "", "", "")]
        public void User_CreateWithData_ReturnJson(string name, string surname, string mail, string avatar)
        {
            var schema = BlogSchema.MakeSchema();
            var request = new QueryRequest
            {
                Query = @"
                mutation AddUser($name: String!, $surname: String!, $mail: String!, $avatar: String!) {
                    addUser(name: $name, surname: $surname, mail: $mail, avatar: $avatar) {
                        id
                        name
                        surname
                        mail
                        avatar
                    }
                }",

                Variables = new QueryVariables
                {
                    {"name", $"{name}"},
                    {"surname", $"{surname}"},
                    {"mail", $"{mail}"},
                    {"avatar", $"{avatar}"},
                }
            };

            var db = new BlogContext();
            TestHelper.CreateData(db);
            var result = schema.ExecuteQuery(request, db, null, null);
            var user = db.Users.AsEnumerable().Last();

            Assert.Empty(result.Errors);
            Assert.True(user.Id == 5);
            Assert.Equal("Yahiko", user.Name);
            Assert.Equal("Uzumaki", user.Surname);
            Assert.Equal("yahiko@akatsuki.jp", user.Mail);
            Assert.Equal("https://website/to/image/yahiko", user.Avatar);
        }

        [Theory]
        [InlineData(null, "Uzumaki", "yahiko@akatsuki.jp", "https://website/to/image/yahiko")]
        [InlineData("Yahiko", null, "yahiko@akatsuki.jp", "https://website/to/image/yahiko")]
        [InlineData("Yahiko", "Uzumaki", null, "https://website/to/image/yahiko")]
        [InlineData("Yahiko", "Uzumaki", "yahiko@akatsuki.jp", null)]
        [InlineData(null, null, null, null)]
        public void User_DontCreateWithoutVariables_ReturnError(string? name, string? surname, string? mail,
            string? avatar)
        {
            var schema = BlogSchema.MakeSchema();
            var request = new QueryRequest
            {
                Query = @"
                mutation AddUser($name: String!, $surname: String!, $mail: String!, $avatar: String!) {
                    addUser(name: $name, surname: $surname, mail: $mail, avatar: $avatar) {
                        id
                    }
                }",
                Variables = new QueryVariables()
            };

            if (name != null)
                request.Variables.Add("name", $"{name}");
            if (surname != null)
                request.Variables.Add("surname", $"{surname}");
            if (mail != null)
                request.Variables.Add("mail", $"{mail}");
            if (avatar != null)
                request.Variables.Add("avatar", $"{avatar}");

            var db = new BlogContext();
            TestHelper.CreateData(db);
            var result = schema.ExecuteQuery(request, db, null, null);

            Assert.NotEmpty(result.Errors);
            Assert.Equal(4, db.Users.Count());
        }

        [Theory]
        [MemberData(nameof(UserTestData.QueriesWithoutData), MemberType = typeof(UserTestData))]
        public void User_DontCreateWithoutData_ReturnError(string query)
        {
            var schema = BlogSchema.MakeSchema();
            var request = new QueryRequest
            {
                Query = query,
                Variables = new QueryVariables
                {
                    {"name", $"Yahiko"},
                    {"surname", $"Uzumaki"},
                    {"mail", $"mail"},
                    {"avatar", $"avatar link"},
                }
            };

            var db = new BlogContext();
            TestHelper.CreateData(db);
            var result = schema.ExecuteQuery(request, db, null, null);

            Assert.NotEmpty(result.Errors);
            Assert.Equal(4, db.Users.Count());
        }

        #endregion

        #region REMOVE

        public void User_RemoveById_ReturnUsers(User to, User caller)
        {
            var schema = BlogSchema.MakeSchema();
            var request = new QueryRequest
            {    
                Query = 
                    @"mutation RemoveUser($to: User!, $caller: User!) {
                        removeUser(to: $to, caller: $caller) {
                            id
                        }
                    }",
                Variables = new QueryVariables
                {
                    {"to", $"{to}"},
                    {"caller", $"{caller}"}
                }
            };
        }
        
        #endregion

        #region UPDATE

        #endregion
    }
}