﻿using System.Collections.Generic;
using Xunit;

namespace GraphqlTests.Data
{
    public class UserTestData
    {
        public static TheoryData<string> QueriesWithoutData =>
            new TheoryData<string>
            {
                @"
                mutation AddUser($name: String!, $surname: String!, $mail: String!) {
                    addUser(name: $name, surname: $surname, mail: $mail) {
                        id
                    }
                }",
                @"
                mutation AddUser($name: String!, $surname: String!, $avatar: String!) {
                    addUser(name: $name, surname: $surname, avatar: $avatar) {
                        id
                    }
                }",
                @"
                mutation AddUser($name: String!, $mail: String!, $avatar: String!) {
                    addUser(name: $name, mail: $mail, avatar: $avatar) {
                        id
                    }
                }",
                @"
                mutation AddUser($surname: String!, $mail: String!, $avatar: String!) {
                    addUser(surname: $surname, mail: $mail, avatar: $avatar) {
                        id
                    }
                }"
            };


    }
}