﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using EntityGraphQL.Schema;

namespace Backend.Advanced.Models
{
    public class Post
    {
        public uint Id { get; set; }
        public User? Author { get; set; }
        public string Header { get; set; }
        public string Content { get; set; }
        public DateTime? Date { get; set; }
    }
}