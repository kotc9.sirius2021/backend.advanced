﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Backend.Advanced.Models
{
    public class User
    {
        public uint Id { get; set; }
        public string Name { get; set; }
        public string Surname { get; set; }
        public string Mail { get; set; }
        public string Avatar { get; set; }

        public override bool Equals(object? obj)
        {
            if (obj is User user)
            {
                return user.Id == Id;
            }

            return false;
        }

        protected bool Equals(User other)
        {
            return Id == other.Id;
        }

        public override int GetHashCode()
        {
            return HashCode.Combine(Id, Name, Surname, Mail, Avatar);
        }
    }
}