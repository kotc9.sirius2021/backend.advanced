﻿using System;
using System.Collections.Generic;

namespace Backend.Advanced.Models
{
    public class Comment
    {
        public uint Id { get; set; }
        public bool IsReply { get; set; }
        public uint PostId { get; set; }
        public User Author { get; set; }
        public string Content { get; set; }
        public DateTime? Date { get; set; }
        
        //подгрузка ответов
        public List<Comment>? Replies { get; set; }
    }
}