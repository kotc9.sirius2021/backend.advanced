﻿using Backend.Advanced.Models;
using Microsoft.EntityFrameworkCore;

namespace Backend.Advanced.Database
{
    public class BlogContext : DbContext
    {
        public DbSet<User> Users { get; set; }
        public DbSet<Post> Posts { get; set; }
        public DbSet<Comment> Comments { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            //sqlite db
            optionsBuilder.UseSqlite("Data Source=blogs.db");
        }
    }
}