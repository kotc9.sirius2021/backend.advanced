﻿using System;
using System.Net;
using System.IO;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;

/*
 * github/gitlab/telegram - kotC9
 */

namespace Backend.Advanced.Controllers
{
    [Route("[controller]")]
    public class ContentController : ControllerBase
    {
        private readonly IWebHostEnvironment _appEnvironment;

        public ContentController(IWebHostEnvironment appEnvironment)
        {
            _appEnvironment = appEnvironment;
        }

        [HttpGet("{name}")]
        public object Get(string name)
        {
            try
            {
                var filePath = Path.Combine(_appEnvironment.ContentRootPath, $"Content/{name}");
                if (System.IO.File.Exists(filePath))
                {
                    return PhysicalFile(filePath, "image/jpg", name);
                }
                else
                {
                    return HttpStatusCode.NotFound;
                }
            }
            catch
            {
                return HttpStatusCode.InternalServerError;
            }
        }
    }
}