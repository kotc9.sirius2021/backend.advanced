﻿using System;
using System.Net;
using Backend.Advanced.Database;
using EntityGraphQL;
using EntityGraphQL.Schema;
using Microsoft.AspNetCore.Mvc;

/*
 * github/gitlab/telegram - kotC9
 */

namespace Backend.Advanced.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class QueryController : ControllerBase
    {
        private readonly BlogContext _dbContext;
        private readonly SchemaProvider<BlogContext> _schemaProvider;

        public QueryController(BlogContext dbContext, SchemaProvider<BlogContext> schemaProvider)
        {
            _dbContext = dbContext;

            _schemaProvider = schemaProvider;
        }

        [HttpGet]
        public object Get([FromQuery] string query)
        {
            return RunDataQuery(new QueryRequest {Query = query});
        }

        [HttpPost]
        public object Post([FromBody] QueryRequest query)
        {
            return RunDataQuery(query);
        }

        private object RunDataQuery(QueryRequest query)
        {
            try
            {
                var data = _schemaProvider.ExecuteQuery(query, _dbContext, HttpContext.RequestServices, null);
                return data;
            }
            catch (Exception)
            {
                return HttpStatusCode.InternalServerError;
            }
        }
    }
}