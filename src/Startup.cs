using System.Linq;
using Backend.Advanced.data;
using Backend.Advanced.Database;
using Backend.Advanced.mutations;
using Backend.Advanced.schemes;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;

/*
 * github/gitlab/telegram - kotC9
 */

namespace Backend.Advanced
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        public void ConfigureServices(IServiceCollection services)
        {
            services.AddControllers().AddNewtonsoftJson();
            services.AddControllers();
            
            services.AddDbContext<BlogContext>();
            services.AddSingleton(BlogSchema.MakeSchema());
            services.AddSingleton(new BlogMutationsHelper());
        }

        public void Configure(IApplicationBuilder app, IWebHostEnvironment env, BlogContext db)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseDefaultFiles();
            app.UseStaticFiles();

            app.UseHttpsRedirection();

            app.UseRouting();

            app.UseEndpoints(endpoints => { endpoints.MapControllers(); });

            CreateData(db);
        }

        private void CreateData(BlogContext db)
        {
            db.Database.EnsureDeleted();
            db.Database.EnsureCreated();

            db.Users.AddRange(
                DatabaseData.Users.Select(c =>
                {
                    c.Avatar = $"https://localhost:5001{c.Avatar}";
                    return c;
                }));

            db.Posts.AddRange(DatabaseData.Posts);
            db.Comments.AddRange(DatabaseData.Comments);
            db.SaveChanges();
        }
    }
}