﻿using System;
using System.Collections.Generic;
using Backend.Advanced.Models;

/*
 * github/gitlab/telegram - kotC9
 */

namespace Backend.Advanced.data
{
    public class DatabaseData
    {
        public static List<User> Users = new List<User>
        {
            new User
            {
                Name = "Itachi",
                Surname = "Uchiha",
                Avatar = "/content/itachi.jpg",
                Mail = "itachi@akatsuki.jp",
                
            },
            new User
            {
                Name = "Sasuke",
                Surname = "Uchiha",
                Avatar = "/content/sasuke.jpg",
                Mail = "sasuke@leaf.village.jp"
            },
            new User
            {
                Name = "Deidara",
                Surname = "Tsukiri",
                Avatar = "/content/deidara.jpg",
                Mail = "itachi@akatsuki.jp"
            },
            new User
            {
                Name = "Nagato",
                Surname = "Uzumaki",
                Avatar = "/content/nagato.jpg",
                Mail = "nagato@akatsuki.jp"
            },
        };

        public static List<Post> Posts = new List<Post>
        {
            new Post
            {
                Author = Users[0],
                Header = "заголовок",
                Content = "контент от итачи id 1",
                Date = DateTime.Today
            },
            new Post
            {
                Author = Users[0],
                Header = "заголовок 2",
                Content = "контент 2 от итачи id 1",
                Date = DateTime.Today
            },
            new Post
            {
                Author = Users[1],
                Header = "заголовок 3",
                Content = "контент 3 от саске id 2",
                Date = DateTime.Today
            },
        };
        
        public static List<Comment> Comments = new List<Comment>
        {
            new Comment
            {
                PostId = 1,
                Author = Users[0],
                Content = "комментарий от итачи id 1",
                Replies = new List<Comment>
                {
                    new Comment
                    {
                        IsReply = true,
                        PostId = 1,
                        Author = Users[0],
                        Content = "комментарий 2 от итачи id 1",
                    },
                    new Comment
                    {
                        IsReply = true,
                        PostId = 1,
                        Author = Users[1],
                        Content = "ответ на комментарий от саске id 2",
                        Replies = new List<Comment>
                        {
                            new Comment
                            {
                                IsReply = true,
                                PostId = 1,
                                Author = Users[2],
                                Content = "ответ на ответ от дейдары id 3",
                            }
                        },
                    },
                    new Comment
                    {
                        IsReply = true,
                        PostId = 1,
                        Author = Users[3],
                        Content = "ответ на комментарий от нагато id 4",
                    }
                }
            },
            new Comment
            {
                PostId = 1,
                Author = Users[3],
                Content = "комментарий от нагато id 4",
            },
            new Comment
            {
                PostId = 2,
                Author = Users[2],
                Content = "комментарий от дейдары к посту 2 id 3",
            }
        };
    }
}