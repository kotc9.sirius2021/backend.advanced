﻿using System.Linq;
using Backend.Advanced.Database;
using Backend.Advanced.mutations;
using EntityGraphQL.Schema;
/*
 * github/gitlab/telegram - kotC9
 */
namespace Backend.Advanced.schemes
{
    public class BlogSchema
    {
        public static SchemaProvider<BlogContext> MakeSchema()
        {
            var blogSchema = SchemaBuilder.FromObject<BlogContext>();
            
            blogSchema.AddMutationFrom(new UserMutations());

            //filter
            blogSchema.ReplaceField("comments", 
                db => db.Comments.Where(c => c.IsReply==false), 
                "List of comments, not replies");
            
            blogSchema.AddMutationFrom(new PostMutations());
            blogSchema.AddMutationFrom(new UserMutations());
            blogSchema.AddMutationFrom(new CommentsMutations());
            
            return blogSchema;
        }
    }
}