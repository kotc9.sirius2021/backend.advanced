﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Linq.Expressions;
using Backend.Advanced.Database;
using Backend.Advanced.Models;
using EntityGraphQL;
using EntityGraphQL.Schema;

/*
 * github/gitlab/telegram - kotC9
 */

namespace Backend.Advanced.mutations
{
    public class PostMutations
    {
        /// <summary>
        /// добавление поста
        /// </summary>
        [GraphQLMutation("Add a new post")]
        public Expression<Func<BlogContext, IEnumerable<Post>>> AddPost(BlogContext db, AddPostArgs args,
            GraphQLValidator validator, BlogMutationsHelper helper)
        {
            var author = helper.FindUser(db, args.Author);

            if (author != null)
            {
                var post = new Post
                {
                    Author = author,
                    Header = args.Header,
                    Content = args.Content,
                    Date = args.Date ?? DateTime.Now
                };


                db.Posts.Add(post);
                db.SaveChanges();
            }
            else
            {
                validator.AddError("post data is incorrect");
            }

            if (validator.HasErrors)
                return null;
            return ctx => ctx.Posts.AsEnumerable();
        }

        /// <summary>
        /// удаление поста
        /// удалить пост может только его создатель
        /// </summary>
        [GraphQLMutation("Remove a post")]
        public Expression<Func<BlogContext, IEnumerable<Post>>> RemovePost(BlogContext db, RemovePostArgs args,
            GraphQLValidator validator, BlogMutationsHelper helper)
        {
            var (valid, post) = helper.ValidatePost(db, args.Caller, args.Post);

            if (valid)
            {
                db.Posts.Remove(post);
                db.SaveChanges();
            }
            else
            {
                validator.AddError("post data is incorrect");
            }

            if (validator.HasErrors)
                return null;
            return ctx => ctx.Posts.AsEnumerable();
        }

        /// <summary>
        /// обновление поста
        /// обновлять пост может только тот кто его создал
        /// </summary>
        [GraphQLMutation("Update a post")]
        public Expression<Func<BlogContext, IEnumerable<Post>>> UpdatePost(BlogContext db, UpdatePostArgs args,
            GraphQLValidator validator, BlogMutationsHelper helper)
        {
            var (valid, post) = helper.ValidatePost(db, args.Caller.Author, args.To);
            if (valid)
            {
                var caller = args.Caller;

                if (!string.IsNullOrEmpty(caller.Header))
                    post.Header = caller.Header;

                if (!string.IsNullOrEmpty(caller.Content))
                    post.Content = caller.Content;

                if (caller.Date != null)
                    post.Date = caller.Date;

                db.SaveChanges();
            }
            else
            {
                validator.AddError("post data is incorrect");
            }

            if (validator.HasErrors)
                return null;
            return ctx => ctx.Posts.AsEnumerable();
        }

        [MutationArguments]
        public class AddPostArgs
        {
            [Required(AllowEmptyStrings = false, ErrorMessage = "Author data is required")]
            public User Author { get; set; }

            [Required(AllowEmptyStrings = false, ErrorMessage = "Header is required")]
            public string Header { get; set; }

            [Required(AllowEmptyStrings = false, ErrorMessage = "Content data is required")]
            public string Content { get; set; }

            public DateTime? Date { get; set; }
        }

        [MutationArguments]
        public class RemovePostArgs
        {
            [Required(AllowEmptyStrings = false, ErrorMessage = "Post data is required")]
            public Post Post { get; set; }

            [Required(AllowEmptyStrings = false, ErrorMessage = "User caller is required")]
            public User Caller { get; set; }
        }

        [MutationArguments]
        public class UpdatePostArgs
        {
            [Required(AllowEmptyStrings = false, ErrorMessage = "Post to which to update is required")]
            public Post To { get; set; }

            [Required(AllowEmptyStrings = false, ErrorMessage = "Post from which to update is required")]
            public Post Caller { get; set; }
        }
    }
}