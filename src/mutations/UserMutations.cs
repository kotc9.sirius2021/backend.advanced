﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Linq.Expressions;
using Backend.Advanced.Database;
using Backend.Advanced.Models;
using EntityGraphQL;
using EntityGraphQL.Schema;
using Microsoft.EntityFrameworkCore;
/*
 * github/gitlab/telegram - kotC9
 */
namespace Backend.Advanced.mutations
{
    public class UserMutations
    {
        /// <summary>
        /// добавление пользователя
        /// по тз ничего не сказано о том каких пользователей добавлять,
        /// поэтому не сравнивается почта и все такое
        /// </summary>
        [GraphQLMutation("Add a new user")]
        public Expression<Func<BlogContext, IEnumerable<User>>> AddUser(BlogContext db, AddUserArgs addUser)
        {
            db.Users.Add(new User
            {
                Name = addUser.Name,
                Surname = addUser.Surname,
                Avatar = addUser.Avatar,
                Mail = addUser.Mail
            });

            db.SaveChanges();

            
            return ctx => ctx.Users.AsEnumerable();
        }

        /// <summary>
        /// удаление пользователя
        /// удалять пользователя может только тот кто его создал
        /// </summary>
        [GraphQLMutation("Remove a user")]
        public Expression<Func<BlogContext, IEnumerable<User>>> RemoveUser(BlogContext db, ActionUserArgs userArgs,
            GraphQLValidator validator, BlogMutationsHelper helper)
        {
            var valid = helper.ValidateUser(db, userArgs.Caller, userArgs.To);
            var user = helper.FindUser(db, userArgs.To);
            if (valid && user != null)
            {
                db.Users.Remove(user);
                db.SaveChanges();
            }
            else
            {
                validator.AddError("user data is incorrect");
            }

            if (validator.HasErrors)
                return null;
            return ctx => db.Users.AsEnumerable();
        }

        /// <summary>
        /// обновление пользователя
        /// обновлять пользователя может только тот кто его создал
        /// </summary>
        [GraphQLMutation("Update a user")]
        public Expression<Func<BlogContext, IEnumerable<User>>> UpdateUser(BlogContext db, ActionUserArgs userArgs,
            GraphQLValidator validator, BlogMutationsHelper helper)
        {
            var valid = helper.ValidateUser(db, userArgs.Caller, userArgs.To);
            var user = helper.FindUser(db, userArgs.To);
            if (valid && user != null)
            {
                var caller = userArgs.Caller;

                //обновление только того что можно обновить
                if (!string.IsNullOrEmpty(caller.Name))
                    user.Name = caller.Name;

                if (!string.IsNullOrEmpty(caller.Surname))
                    user.Surname = caller.Surname;

                if (!string.IsNullOrEmpty(caller.Avatar))
                    user.Avatar = caller.Avatar;

                if (!string.IsNullOrEmpty(caller.Mail))
                    user.Mail = caller.Mail;

                db.SaveChanges();
            }
            else
            {
                validator.AddError("user data is incorrect");
            }

            if (validator.HasErrors)
                return null;
            return ctx => db.Users.AsEnumerable();
        }

        [MutationArguments]
        public class AddUserArgs
        {
            [Required(AllowEmptyStrings = false, ErrorMessage = "User Name is required")]
            public string Name { get; set; }

            [Required(AllowEmptyStrings = false, ErrorMessage = "User Surname is required")]
            public string Surname { get; set; }

            [Required(AllowEmptyStrings = false, ErrorMessage = "User Mail is required")]
            public string Mail { get; set; }

            public string Avatar { get; set; }
        }

        [MutationArguments]
        public class ActionUserArgs
        {
            [Required(AllowEmptyStrings = false, ErrorMessage = "User to action is required")]
            public User To { get; set; }
            
            [Required(AllowEmptyStrings = false, ErrorMessage = "User caller is required")]
            public User Caller { get; set; }
        }
    }
}