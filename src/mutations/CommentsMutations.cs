﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Linq.Expressions;
using Backend.Advanced.Database;
using Backend.Advanced.Models;
using Backend.Advanced.schemes;
using EntityGraphQL;
using EntityGraphQL.Schema;
/*
 * github/gitlab/telegram - kotC9
 */
namespace Backend.Advanced.mutations
{
    public class CommentsMutations
    {
        /// <summary>
        /// добавление комментария
        /// </summary>
        [GraphQLMutation("Add a new Comment")]
        public Expression<Func<BlogContext, IEnumerable<Comment>>> AddComment(BlogContext db, AddCommentArgs args,
            GraphQLValidator validator, BlogMutationsHelper helper)
        {
            var author = helper.FindUser(db, args.Author);

            //автор должен быть существующим, а также пост должен существовать
            if (author != null && helper.PostById(db, args.PostId) != null)
            {
                var comment = new Comment
                {
                    PostId = args.PostId,
                    Author = author,
                    Content = args.Content,
                    Date = args.Date ?? DateTime.Now,
                    Replies = args.Replies ?? new List<Comment>()
                };

                db.Comments.Add(comment);
                db.SaveChanges();
            }
            else
            {
                validator.AddError("comment data is incorrect");
            }

            if (validator.HasErrors)
                return null;
            return ctx => ctx.Comments.AsEnumerable();
        }

        /// <summary>
        /// добавление ответа на комментарий
        /// </summary>
        [GraphQLMutation("Add a new Reply")]
        public Expression<Func<BlogContext, IEnumerable<Comment>>> AddReply(BlogContext db, AddReplyArgs args,
            GraphQLValidator validator, BlogMutationsHelper helper)
        {
            var toComment = helper.FindComment(db, args.ToComment);
            var author = helper.FindUser(db, args.Author);

            if (toComment != null && author != null)
            {
                toComment.Replies ??= new List<Comment>();
                var reply = new Comment
                {
                    IsReply = true,
                    Author = author,
                    Content = args.Content,
                    Date = args.Date ?? DateTime.Now,
                    Replies = args.Replies ?? new List<Comment>(),
                    PostId = toComment.PostId
                };
                toComment.Replies.Add(reply);
                db.SaveChanges();
            }
            else
            {
                validator.AddError("comment data is incorrect");
            }


            if (validator.HasErrors)
                return null;
            return ctx => ctx.Comments.AsEnumerable();
        }

        /// <summary>
        /// удаление комментария
        /// удалить пост может только его создатель и автор поста
        /// </summary>
        [GraphQLMutation("Remove a Comment")]
        public Expression<Func<BlogContext, IEnumerable<Comment>>> RemoveComment(BlogContext db, RemoveCommentArgs args,
            GraphQLValidator validator, BlogMutationsHelper helper)
        {
            var (valid, comment) = helper.ValidateComment(db, args.Caller, args.Comment);

            if (valid)
            {
                var post = helper.PostById(db, comment.PostId);
                var caller = helper.FindUser(db, args.Caller);
                //удалять может только автор поста или автор комментария
                if (post != null && Equals(post.Author, caller) || Equals(comment.Author, caller))
                {
                    helper.RemoveComment(db, comment);
                }
                else
                {
                    validator.AddError("user data is incorrect");
                }
            }
            else
            {
                validator.AddError("comment data is incorrect");
            }

            if (validator.HasErrors)
                return null;

            return ctx => ctx.Comments.AsEnumerable();
        }

        /// <summary>
        /// обновление поста
        /// обновлять пост может только тот кто его создал
        /// </summary>
        [GraphQLMutation("Update a Comment")]
        public Expression<Func<BlogContext, IEnumerable<Comment>>> UpdateComment(BlogContext db, UpdateCommentArgs args,
            GraphQLValidator validator, BlogMutationsHelper helper)
        {
            var (valid, comment) = helper.ValidateComment(db, args.Caller.Author, args.To);

            if (valid)
            {
                var caller = helper.FindUser(db, args.Caller.Author);

                if (Equals(caller, comment.Author))
                {
                    //изменить можно только контент в сообщении и дату
                    if (!string.IsNullOrEmpty(args.Caller.Content))
                        comment.Content = args.Caller.Content;

                    if (args.Caller.Date != null)
                        comment.Date = args.Caller.Date;

                    db.SaveChanges();
                }
                else
                {
                    validator.AddError("user data is incorrect");
                }
            }
            else
            {
                validator.AddError("comment data is incorrect");
            }

            if (validator.HasErrors)
                return null;
            return ctx => ctx.Comments.AsEnumerable();
        }

        [MutationArguments]
        public class AddCommentArgs
        {
            [Required(AllowEmptyStrings = false, ErrorMessage = "Post ID is required")]
            public uint PostId { get; set; }

            [Required(AllowEmptyStrings = false, ErrorMessage = "Author is required")]
            public User Author { get; set; }

            [Required(AllowEmptyStrings = false, ErrorMessage = "Content is required")]
            public string Content { get; set; }

            public DateTime? Date { get; set; }
            public List<Comment>? Replies { get; set; }
        }

        [MutationArguments]
        public class AddReplyArgs
        {
            [Required(AllowEmptyStrings = false, ErrorMessage = "Comment is required")]
            public Comment ToComment { get; set; }

            [Required(AllowEmptyStrings = false, ErrorMessage = "Author is required")]
            public User Author { get; set; }

            [Required(AllowEmptyStrings = false, ErrorMessage = "Content is required")]
            public string Content { get; set; }

            public DateTime? Date { get; set; }
            public List<Comment>? Replies { get; set; }
        }

        [MutationArguments]
        public class RemoveCommentArgs
        {
            [Required(AllowEmptyStrings = false, ErrorMessage = "Comment data is required")]
            public Comment Comment { get; set; }

            [Required(AllowEmptyStrings = false, ErrorMessage = "User caller is required")]
            public User Caller { get; set; }
        }

        [MutationArguments]
        public class UpdateCommentArgs
        {
            [Required(AllowEmptyStrings = false, ErrorMessage = "Comment to which to update is required")]
            public Comment To { get; set; }

            [Required(AllowEmptyStrings = false, ErrorMessage = "Comment from which to update is required")]
            public Comment Caller { get; set; }
        }
    }
}