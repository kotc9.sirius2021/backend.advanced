﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using Backend.Advanced.Database;
using Backend.Advanced.extensions;
using Backend.Advanced.Models;
using EntityGraphQL.Extensions;
using Microsoft.EntityFrameworkCore;
/*
 * github/gitlab/telegram - kotC9
 */
namespace Backend.Advanced.mutations
{
    public class BlogMutationsHelper
    {
        /// <summary>
        /// валидация пользователей
        /// </summary>
        public bool ValidateUser(BlogContext db, User a, User b)
        {
            if (!Equals(a, b))
                return false;

            db.Refresh();

            return true;
        }

        /// <summary>
        /// валидация поста для удаления/обновления
        /// </summary>
        public (bool, Post p) ValidatePost(BlogContext db, User? caller, Post post)
        {
            if (caller == null)
                return (false, null)!;
            
            db.Refresh();
            var c = FindUser(db, caller);
            var p = FindPost(db, post);
            
            //неверно указан автор в запросе
            if (post.Author != null)
            {
                var a = FindUser(db, post.Author);
                
                return (p != null && Equals(p.Author, c) && Equals(p.Author, a)
                    ? (true, p)
                    : (false, null))!;
            }

            return (p != null && Equals(p.Author, c)
                ? (true, p)
                : (false, null))!;
        }
        
        /// <summary>
        /// валидация комментария 
        /// </summary>
        public (bool, Comment) ValidateComment(BlogContext db, User? caller, Comment comment)
        {
            if (caller == null)
                return (false, null)!;
            
            db.Refresh();
            var u = FindUser(db, caller);
            var c = FindComment(db, comment);
            if (comment.Author != null)
            {
                var a = FindUser(db, comment.Author);
                
                return (c != null && u != null && a != null && Equals(c.Author, a)
                    ? (true, c)
                    : (false, null))!;
            }
            
            return (c != null && u != null
                ? (true, c)
                : (false, null))!;
        }

        public void RemoveComment(BlogContext db, Comment comment)
        {
            RemoveCommentRecursive(db, comment);
            db.SaveChanges();
        }

        private void RemoveCommentRecursive(BlogContext db, Comment comment)
        {
            if (comment.Replies != null && comment.Replies.Any())
            {
                for(var i = comment.Replies.Count-1;i>=0;i--)
                {
                    RemoveCommentRecursive(db, comment.Replies[i]);
                }
            }

            db.Comments.Remove(comment);
        }

        public User? FindUser(BlogContext db, User user)
        {
            return db.Users.FirstOrDefault(u => u.Id == user.Id);
        }

        private bool ExistsUser(BlogContext db, User user)
        {
            return FindUser(db, user) != null;
        }

        private Post? FindPost(BlogContext db, Post post)
        {
            return db.Posts.FirstOrDefault(p => p.Id == post.Id);
        }
        
        public Comment? FindComment(BlogContext db, Comment comment)
        {
            return db.Comments.Where(c => c.Author != null).FirstOrDefault(c => c.Id == comment.Id);
        }

        public Post? PostById(BlogContext db, uint id)
        {
            return db.Posts.FirstOrDefault(p => p.Id == id);
        }

        public List<Comment>? RepliesById(BlogContext db, in uint commentId)
        {
            return db.Comments.FirstOrDefault(c => c.Replies != null && c.Replies.Count > 0)?.Replies;
        }
    }
}